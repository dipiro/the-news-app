//
//  XMLParser.swift
//  RSS Reader
//
//  Created by piro on 2.05.21.
//

import Foundation

struct NetworkManager {

    static func fetch(name: String, comletionHandler: @escaping ((News)->())) {
        guard let url = URL(string: "https://newsapi.org/v2/top-headlines?sources=\(name)&apiKey=5bcb3d6cf0af4d0ebe1740b592a2e7f2") else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let response = response {
                print(response)
            }
            
            if let error = error {
                print(error)
            }
            
            guard let data = data else { return }
            do {
                let decoder = JSONDecoder()
                let newsData = try decoder.decode(News.self, from: data)

                comletionHandler(newsData)
            } catch {
                print(error)
            }
        }.resume()
    }
}

