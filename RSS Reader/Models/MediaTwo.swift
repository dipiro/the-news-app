//
//  MediaTwo.swift
//  RSS Reader
//
//  Created by piro on 3.05.21.
//

import Foundation
import Firebase

struct Media {
    let name: String
    let status: String
    let userID: String
    let ref: DatabaseReference?
    
    init(name: String, status: String, userID: String) {
        self.name = name
        self.status = status
        self.userID = userID
        self.ref = nil
    }
    
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        name = snapshotValue["name"] as! String
        status = snapshotValue["status"] as! String
        userID = snapshotValue["userID"] as! String
        ref = snapshot.ref
    }
    
    func convertToDictionary() -> Any {
        return ["name": name, "status": status, "userID": userID]
    }
    
}
