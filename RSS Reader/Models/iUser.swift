//
//  User.swift
//  RSS Reader
//
//  Created by piro on 1.05.21.
//

import Foundation
import Firebase

struct iUser {
    
    let userID: String
    let userEmail: String
     
    init(user: User) {
        self.userID = user.uid
        self.userEmail = user.email!
    }
}
