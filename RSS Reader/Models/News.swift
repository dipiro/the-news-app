//
//  Article.swift
//  RSS Reader
//
//  Created by piro on 2.05.21.
//

import Foundation

struct News: Codable {
    let articles: [Article]?
}

struct Article: Codable {
    let source: Source?
    let title: String?
    let url: String?
    let urlToImage: String?
}

struct Source: Codable {
    let name: String?
}
