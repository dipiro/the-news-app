//
//  Media.swift
//  RSS Reader
//
//  Created by piro on 1.05.21.
//

import Foundation
import Firebase

struct Media {
    let author: String
    let userID: String
    let ref: DatabaseReference?
    var completed: Bool = false
    
    init(name: String, userID: String) {
        self.author = name
        self.userID = userID
        self.ref = nil
    }
    
    init(snapshot: DataSnapshot) {
        let snapshopValue = snapshot.value as! [String: AnyObject]
        author = snapshopValue["name"] as! String
        userID = snapshopValue["userID"] as! String
        completed = snapshopValue["completed"] as! Bool
        ref = snapshot.ref
    }
}
