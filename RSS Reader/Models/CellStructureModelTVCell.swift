//
//  CellStructureModelTVC.swift
//  News Reader App
//
//  Created by piro on 11.05.21.
//

import Foundation

protocol CellStructureModelTVCell {
    var title: String { get }
}
