//
//  ViewController.swift
//  RSS Reader
//
//  Created by piro on 28.04.21.
//

import UIKit
import Firebase

class MainVC: UIViewController {
    
    private var viewModel = MainViewModel()
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: StartTVCell.identifier, bundle: nil), forCellReuseIdentifier: StartTVCell.identifier)
            tableView.delegate = self
            tableView.dataSource = self
            
        }
    }
    
    var ref: DatabaseReference!
    var user: iUser!
    
    let url = "https://newsapi.org/v2/everything?q=tesla&from=2021-04-02&sortBy=publishedAt&apiKey=5bcb3d6cf0af4d0ebe1740b592a2e7f2"
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        DispatchQueue.global().async { [weak self] in
            guard let currentUser = Auth.auth().currentUser else { return }
            user = iUser(user: currentUser)
            ref = Database.database().reference(withPath: "users").child(String(user.userID)).child("tasks")
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewModel.clearArray()
        self.tableView.reloadData()
        
        self.ref.observe(.value) {  [weak self] (snapshot) in
            
            var _tasks = Array<Media>()
            
            for item in snapshot.children {
                let task = Media(snapshot: item as! DataSnapshot)
                _tasks.append(task)
                
            }
            
            for item in _tasks {
                if item.status == "selected" {
                    NetworkManager.fetch(name: item.name) { (newsData) in
                        self?.viewModel.appendInArray(newsData)
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                            
                        }
                    }
                }
            }
        }
    }
    
    
    @IBAction func hitMediaButtonItem(_ sender: Any) {
        let mediaVC = MediaViewController(nibName: MediaViewController.identifier, bundle: nil)
        mediaVC.modalPresentationStyle = .fullScreen
        
        navigationController?.pushViewController(mediaVC, animated: true)
    }
    
    @IBAction func signOutButtonItem(_ sender: UIBarButtonItem) {
        do {
            try Auth.auth().signOut()
        } catch {
            print(error.localizedDescription)
        }
        dismiss(animated: true, completion: nil)
    }
    
}

extension MainVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return                         viewModel.getCount()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        viewModel.getTitleForHeader(section)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.getNumberOfRows(section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: StartTVCell.identifier, for: indexPath) as? StartTVCell else { return UITableViewCell() }
        
        cell.nameLabel.text = viewModel.getTitleForCell(indexPath)
        

        cell.imageView?.loadImage(url: viewModel.getUrlOfImageForCell(indexPath), completionHandler: { (image) in
            DispatchQueue.main.async {
                cell.imageImage.image = image
                cell.loadingIndicator.stopAnimating()
            }
        })
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mediaVC = WebVC(nibName: WebVC.identifier, bundle: nil)
        mediaVC.modalPresentationStyle = .fullScreen
        mediaVC.urlString = viewModel.getUrlForCell(indexPath)
        
        navigationController?.pushViewController(mediaVC, animated: true)
    }
}


// Mark: -Mark-
extension UIImageView {
    func loadImage(url: String, completionHandler: @escaping (UIImage)->()) {
        let url = URL(string: url)
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url!) {
                if let image = UIImage(data: data) {
                    completionHandler(image)
                }
            }
        }
    }
}
