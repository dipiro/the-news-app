//
//  MediaTVCell.swift
//  RSS Reader
//
//  Created by piro on 30.04.21.
//

import UIKit

class MediaTVCell: UITableViewCell {
    
    static let identifier = "MediaTVCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var switchView: UISwitch!
    @IBOutlet weak var imageLabel: UIImageView!
    
}
