//
//  MediaViewController.swift
//  RSS Reader
//
//  Created by piro on 28.04.21.
//

import UIKit
import Firebase

class MediaViewController: UIViewController {
    
    static let identifier = "MediaViewController"
    
    var user: iUser!
    var ref: DatabaseReference!
    var tasks = Array<Media>()
    
    var viewModel = MediaViewModel()
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: MediaTVCell.identifier, bundle: nil), forCellReuseIdentifier: MediaTVCell.identifier)
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let currentUser = Auth.auth().currentUser else { return }
        user = iUser(user: currentUser)
        ref = Database.database().reference(withPath: "users").child(String(user.userID)).child("tasks")
        
        //        for array in arraysMedia {
        //            let name = array[0]
        //            let status = array[1]
        //            let task = MediaTwo(name: name, status: status, userID: user.userID)
        //            let taskRef = self.ref.child(task.name.lowercased())
        //            taskRef.setValue(task.convertToDictionary())
        //        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.ref.observe(.value) {  [weak self] (snapshot) in
            
            var _tasks = Array<Media>()
            
            for item in snapshot.children {
                let task = Media(snapshot: item as! DataSnapshot)
                _tasks.append(task)
            }
            
            self?.tasks = _tasks
            self?.tableView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ref.removeAllObservers()
        print(123)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        print(456)
    }
    
    
    @objc func switchDidChange(_ sender: UISwitch) {
        print(sender.tag)
        if sender.isOn == true {
            let task = tasks[sender.tag]
            print(sender.tag)
            task.ref?.updateChildValues(["status": "selected"])
            
        } else if sender.isOn == false {
            let task = tasks[sender.tag]
            task.ref?.updateChildValues(["status": "unselected"])
        }
    }
    
}

extension MediaViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MediaTVCell.identifier, for: indexPath) as? MediaTVCell else { return UITableViewCell()}
        
        let task = tasks[indexPath.row]
        cell.nameLabel.text = task.name
        
        cell.imageLabel.image = viewModel.getImageForCell(indexPath)
  
        
        if task.status == "unselected" {
            cell.switchView.setOn(false, animated: true)
        } else {
            cell.switchView.setOn(true, animated: true)
        }
        
        cell.switchView.tag = indexPath.row
        cell.switchView.addTarget(self, action: #selector(switchDidChange(_:)), for: .valueChanged)
        
        return cell
    }
}
