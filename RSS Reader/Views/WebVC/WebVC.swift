//
//  WebVC.swift
//  News Reader App
//
//  Created by piro on 10.05.21.
//

import UIKit
import WebKit

class WebVC: UIViewController {
    
    static let identifier = "WebVC"

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var urlTextField: UITextField!
    
    var urlString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        urlTextField.text = urlString
        
        self.navigationController?.navigationItem.titleView = UITextField()
        
        self.navigationItem.titleView = UITextField()
        
        let url = URL(string: urlString)
        let request = URLRequest(url: url!)
        
        webView.load(request)
        
        webView.allowsBackForwardNavigationGestures = true
        // Do any additional setup after loading the view.
    }
}
