//
//  LoginVC.swift
//  RSS Reader
//
//  Created by piro on 1.05.21.
//

import Foundation
import UIKit
import Firebase

class LoginVC: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    
    var user: iUser!
    var ref: DatabaseReference!
    private var viewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        ref = Database.database().reference(withPath: "users")
        
        Auth.auth().addStateDidChangeListener { [weak self] (auth, user) in
            if user != nil {
                self?.performSegue(withIdentifier: "toStartScreen", sender: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        emailTextField.text = ""
        passwordTextField.text = ""
    }
    
    
    @IBAction func loginPressed(_ sender: UIButton) {
        guard let email = emailTextField.text,
              let password = passwordTextField.text,
              email != "",
              password != "" else  { return }
        
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] (userResult, error) in
            if error != nil {
                print(error?.localizedDescription)
                return
            }
            if userResult != nil {
                self?.performSegue(withIdentifier: "toStartScreen", sender: nil)
                return
            }
        }
    }
    @IBAction func registerPressed(_ sender: UIButton) {
        guard let email = emailTextField.text,
              let password = passwordTextField.text,
              email != "",
              password != "" else  { return }
        
        Auth.auth().createUser(withEmail: email, password: password) { [weak self] (user, error) in
            guard error == nil, user != nil else {
                print(error?.localizedDescription)
                return
            }
            let userRef = self?.ref.child((user?.user.uid)!)
            userRef?.setValue(["email": user?.user.email])
            
            guard let currentUser = Auth.auth().currentUser else { return }
            self?.user = iUser(user: currentUser)
            self?.ref = Database.database().reference(withPath: "users").child(String((self?.user.userID)!)).child("tasks")
            
            for array in self!.viewModel.getMediaArrays() {
                let name = array[0]
                let status = array[1]
                let task = Media(name: name, status: status, userID: (self?.user.userID)!)
                let taskRef = self!.ref.child(task.name.lowercased())
                taskRef.setValue(task.convertToDictionary())
            }
        }
    }
}

extension LoginVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
