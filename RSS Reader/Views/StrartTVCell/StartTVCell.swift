//
//  StartTVCell.swift
//  News Reader App
//
//  Created by piro on 10.05.21.
//

import UIKit

class StartTVCell: UITableViewCell {

    static let identifier = "StartTVCell"
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageImage: UIImageView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
//    weak var viewModel {
//        willSet(viewModel) {
//            
//        }
//    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageImage.layer.cornerRadius = 12
        
        loadingIndicator.startAnimating()
        loadingIndicator.style = .large
        loadingIndicator.color = .black
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageImage.image = nil
    }
    
}
