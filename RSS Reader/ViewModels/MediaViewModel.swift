//
//  MediaViewModel.swift
//  News Reader App
//
//  Created by piro on 12.05.21.
//

import Foundation
import UIKit

class MediaViewModel {
    let mediaImages = [UIImage(named: "ABC-News"),
                       UIImage(named: "BBC-News"),
                       UIImage(named: "Engadget"),
                       UIImage(named: "ESPN"),
                       UIImage(named: "TechCrunch")]
    
    func getImageForCell(_ indexPath: IndexPath) -> UIImage {
        let image = mediaImages[indexPath.row]
        guard let imageImage = image else {
            return UIImage(systemName: "nosign")!
        }
        return imageImage
    }
    
}
