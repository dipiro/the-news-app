//
//  MainViewModel.swift
//  News Reader App
//
//  Created by piro on 11.05.21.
//

import Foundation

class MainViewModel {
    
    var array: [News] = []
    
    func appendInArray(_ array: News) {
        self.array.append(array)
    }
    
    func getCount() -> Int {
        return array.count
    }
    
    func clearArray() {
        self.array.removeAll()
    }
    
    func getTitleForHeader(_ section: Int) -> String {
        let news = array[section]
        
        guard let name = news.articles?.first?.source?.name  else {
            return "Error"
        }
        
        return name
    }
    
    func getNumberOfRows(_ section: Int) -> Int {
        let news = array[section]
        
        guard let number = news.articles?.count else {
            return 0
        }
        
        return number
    }
    
    func getTitleForCell(_ indexPath: IndexPath) -> String {
        let news = array[indexPath.section]
        let title = news.articles?[indexPath.row].title
        
        return title ?? "Error"
    }
    
    func getUrlOfImageForCell(_ indexPath: IndexPath) -> String {
        let news = array[indexPath.section]
        let urlOfImage = news.articles?[indexPath.row].urlToImage
        
        return urlOfImage ?? "Error"
    }
    
    func getUrlForCell(_ indexPath: IndexPath) -> String {
        let news = array[indexPath.section]
        let url = news.articles?[indexPath.row].url
        
        return url ?? "Error"
    }
    
}
