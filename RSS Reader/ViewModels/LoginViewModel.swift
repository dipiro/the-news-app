//
//  ViewModel.swift
//  RSS Reader
//
//  Created by piro on 1.05.21.
//

import Foundation



class LoginViewModel {
    let mediaArrays: [[String]] = [["ABC-News", "unselected"],
                                   ["BBC-News", "unselected"],
                                   ["Engadget", "unselected"],
                                   ["ESPN", "unselected"],
                                   ["TechCrunch", "unselected"]]
    
    func getMediaArrays() -> [[String]] {
        return mediaArrays
    }
    
}
